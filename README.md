A quick adaptation of Mike Bostock's force-directed graph showing character co-occurence in *Les Misérables*.  In this version, the character names are displayed.  This is accomplished by wrapping both circles and text svg components within a group svg component. 

Compare to the [original diagram](https://bl.ocks.org/mbostock/4062045) by Mike Bostock.

forked from <a href='http://bl.ocks.org/heybignick/'>heybignick</a>'s block: <a href='http://bl.ocks.org/heybignick/3faf257bbbbc7743bb72310d03b86ee8'>D3.js v4 Force Directed Graph with Labels</a>